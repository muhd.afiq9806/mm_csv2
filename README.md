# Mobile Mapping CSV Edit

## Requirement
- Make sure to install python. (Especially Windows OS)
- Install pandas `pip install pandas`

## How to use
- Download this git in the folder that contains the csv file.
- Make sure the script and the csv file is in the same folder.
- Open the command prompt in the same directory as the script.
- Run the script:
`python csv_edit.py -c filename.csv -f filename -d dirname` or `python3 csv_edit.py -c filename.csv -f filename -d dirname`
    - c: csv file
    - f: naming to define for filename column in the csv
    - d: directory name (eg: KDEBpano)

- A new file is should be created in the same folder as this script with a new name. eg: filename_edit.csv

- Contact Afiq if there is troubleshooting needed.
