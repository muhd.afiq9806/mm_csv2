from tkinter import image_names
import pandas as pd
import argparse
import numpy as np
import os

parser = argparse.ArgumentParser(description='Edit mobile mapping CSV file.')
parser.add_argument(
        '-c', '--csvFile',
        help='Path to the csv file.',
        type=str,
        default=os.getcwd()
)
parser.add_argument(
        '-f', '--fileName',
        help='The file naming.',
        type=str,
        default=os.getcwd()
)
parser.add_argument(
        '-d', '--dirname',
        help='The directory naming.',
        type=str,
        default=os.getcwd()
)

args = parser.parse_args()

#Declare variable from argparse
csvFile = args.csvFile
fileName = args.fileName
dirName = args.dirname
# x = csvFile.split(".")

# Read CSV file
df = pd.read_csv(csvFile)

# pd.set_option('display.max_rows', None)  # Show all rows
# pd.set_option('display.max_columns', None)  # Show all columns

# Rename the column name respective to the reference given to upload into QGIS
df = df.rename(columns = {'lon' :'coordx', 'lat':'coordy', 'height':'altitude', 'gpsDate':'stamp'}, inplace = False)

# Counter for adding the Frame column
frame=[]

# Reorganize the column
# ---------------------------------------------------
df.drop('id', inplace=True, axis=1)
df.drop('tourName', inplace=True, axis=1)
df.drop('localDate', inplace=True, axis=1)
df.drop('active', inplace=True, axis=1)
df.drop('speed', inplace=True, axis=1)
df.drop('road', inplace=True, axis=1)       # Delete Column
df.drop('poiPoint', inplace=True, axis=1)
df.drop('poiLine', inplace=True, axis=1)
df.drop('deviceId', inplace=True, axis=1)
df.drop('haveFix', inplace=True, axis=1)
df.drop('releaseMode', inplace=True, axis=1)
df.drop('camSn', inplace=True, axis=1)
# --------------------------------------------------
df = df[df['heading']!=0]
#---------------------------------------------------
df.insert(1,column="gid",value=" ")
df.insert(4,column="dirname",value=dirName)
df.insert(5,column="filename",value=fileName)
df.insert(9,column="label",value=" ")
df.insert(10,column="durum",value=" ")

# df['imgId']= df['imgId'].astype(str)+".jpeg"

num_col = df.shape
for i in range(num_col[0]):
    i+=1
    # print(i)
    frame.append(i)
# print(frame)

df['tourPath'] = df['tourPath'].str.split('\\').str[-1]
df['imgname'] = df['tourPath'] + '-' + df['imgId'].astype(str)+".jpeg"
df.drop('tourPath', inplace=True, axis=1)
df.drop('imgId', inplace=True, axis=1)

df.insert(0,column="frame",value=frame)

# Moving 'Column3' to the front
column_to_move = 'stamp'
column = df.pop(column_to_move)
df.insert(13, column_to_move, column)

column_to_move = 'coordx'
column = df.pop(column_to_move)
df.insert(2, column_to_move, column)

column_to_move = 'heading'
column = df.pop(column_to_move)
df.insert(6, column_to_move, column)

column_to_move = 'imgname'
column = df.pop(column_to_move)
df.insert(7, column_to_move, column)

column_to_move = 'pitch'
column = df.pop(column_to_move)
df.insert(11, column_to_move, column)

new_fileName = fileName+"_edit.csv"
print("Done! Filename is "+new_fileName)
df.to_csv(new_fileName, header=True, index=False)
